# MiniFit

Copyright (C) 2023-- Michał Kopczyński

MiniFit is a Python package that uses ``scipy.optimize.curve_fit()`` to find optimal parameters of a chosen model function that best resembles the data. ``Popt`` found will minimize ``model_function(xdata, *popt) - ydata``.

Documentation:
https://minifit.readthedocs.io/en/latest/index.html
