#!/usr/bin/env python3


# -*- coding: utf-8 -*-
# Copyright (C) 2023-- Michał Kopczyński
#
# This file is part of MiniFit.
#
# MiniFit is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# MiniFit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>
#

"""Example MiniFit script

For detailed documentation and usage instructions, please refer to the MiniFit package documentation:
https://minifit.readthedocs.io/en/latest/
"""

# pylint: skip-file

import minifit


# Define a user-defined polynomial model
def user_poly(x, *args, **kwargs):
    """User-defined polynomial model"""
    a, b, c, d, e, f, g, h = args
    return (
        a * x**7
        + b * x**6
        + c * x**5
        + d * x**4
        + e * x**3
        + f * x**2
        + g * x
        + h
    )


# Define a quadratic polynomial model
def quadratic_poly(x, *args, **kwargs):
    """Quadratic polynomial model"""
    a, b, c = args
    return a * x**2 + b * x + c


# Define an auto_range for user-defined polynomial model
a_range = [
    (-100, 100),
    (-300, 300),
    (-300, -300),
    (-300, 300),
    (-300, 300),
    (-300, 300),
    (-300, 300),
    (-300, 300),
]

# Create a UserFit instance with user-defined model
poly_user_1 = minifit.UserFit(
    "example.dat",
    model=user_poly,
    num_param=8,
    auto_guess=True,
    precision=0.01,
    auto_range=a_range,
)
poly_user_1()  # Run the optimization

# Create another UserFit instance with quadratic polynomial model
poly_user_1 = minifit.UserFit(
    "example.dat",
    model=quadratic_poly,
    num_param=3,
    type_of_fit="Quadratic Polynomial",
    label_type="Quadratic_Poly",
)
poly_user_1()  # Run the optimization
