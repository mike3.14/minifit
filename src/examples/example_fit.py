#!/usr/bin/env python3


# -*- coding: utf-8 -*-
# Copyright (C) 2023-- Michał Kopczyński
#
# This file is part of MiniFit.
#
# MiniFit is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# MiniFit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>
#

"""Example MiniFit script

For detailed documentation and usage instructions, please refer to the MiniFit package documentation:
https://minifit.readthedocs.io/en/latest/
"""

# pylint: skip-file

import minifit

# Example 1: Polynomial Fit
poly_1 = minifit.PolyFit(
    "example.dat",
    order=7,
    state_info=[
        "info about state 1",
        "info about state 2",
        "info about state 3",
    ],
)  # reads the data from ``exp.dat`` and sets properties of the fit with keyword arguments, if they are not passed, then default values are used.
# ``order=7`` sets fitting order to ax^7 + bx^6 + ...
poly_1()  # Performs the optimization

# Example 2: Gauss Fit
gauss_1 = minifit.GaussFit(
    "gauss_data.txt"
)  # reads data from ``gauss_data.txt`` and uses default properties
gauss_1()  # Performs the optimization

# Example 3: Exponential Fit
exp_1 = minifit.ExponentialFit(
    "exp.dat", guess=(1, 1, 1), x_label="time [s]", y_label="temperature [°C]"
)  # reads data, sets guess for ``a * np.exp(b * x) + c``, where a,b,c = 1
# sets label descriptions and units used
exp_1()  # Performs the optimization

# Example 4: Polynomial Fit with Custom Guess
poly_2 = minifit.PolyFit(
    "poly.dat", guess=(-3.0, 5.0, 45.0), order=2
)  # reads data, sets guess
poly_2()  # Performs the optimization

# Example 5: Polynomial Fit with Auto Guess
poly_2 = minifit.PolyFit(
    "foo_data.txt", order=7, auto_guess=True, precision=0.1
)  # reads data, sets order of the model polynomial, ``auto_guess=True``
# meaning MiniFit will search for a good guess itself, ``precision=0.1`` - acceptable square root error of the fit will be 0.1 or less
poly_2()  # Performs the optimization
