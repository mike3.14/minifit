#!/usr/bin/env python3


# -*- coding: utf-8 -*-
# Copyright (C) 2023-- Michał Kopczyński
#
# This file is part of MiniFit.
#
# MiniFit is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
#
# MiniFit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>
#

"""Example MiniFit script

For detailed documentation and usage instructions, please refer to the MiniFit package documentation:
https://minifit.readthedocs.io/en/latest/
"""

# pylint: skip-file

import minifit

# Example 1: Lennard-Jones Fit with Default Settings
# Reads data from 'foo_data.txt', performs an automatic guess, applies a precision of 5, and includes a shift of data
l_j = minifit.LennardJonesFit(
    "foo_data.txt", auto_guess=True, precision=5, shift=True
)

# Example 2: Lennard-Jones Fit with Custom ``auto_range``
# Reads data from 'foo_data.txt', performs an automatic guess within specified boundaries,
# applies a precision of 5, and includes a shift
l_j2 = minifit.LennardJonesFit(
    "foo_data.txt",
    auto_guess=True,
    auto_range=((-40, 40), (-40, 40)),
    precision=5,
    shift=True,
)

# Example 3: Morse Potential Fit
# Reads data from 'foo_data.txt', performs an automatic guess, applies a precision of 0.01, and excludes a shift
morse = minifit.MorseFit(
    "foo_data.txt",
    auto_guess=True,
    precision=0.01,
    shift=False,
)

# Example 4: Morse Potential Fit with Custom Auto Range
# Reads data from 'foo_data.txt', performs an automatic guess, specifies custom ``auto_range``, applies a precision of 0.01, and excludes a shift
# sets labels and units
morse2 = minifit.MorseFit(
    "foo_data.txt",
    auto_guess=True,
    auto_range=[
        (-5.0, 5.0),  # re
        (-5.0, 5.0),  # de
        (-10.0, 10.0),  # b0
        (-0.1, 0.1),  # b1
        (-0.1, 0.1),  # b2
        (-0.1, 0.1),  # b3
        (-0.1, 0.1),  # b4
        (-0.1, 0.1),  # b5
        (-0.1, 0.1),  # b6
    ],
    precision=0.01,
    shift=False,
    x_label="r[A]",
    y_label="E[eV]",
)


# Run the fitting optimizations for each created object
l_j()
l_j2()
morse()
morse2()
