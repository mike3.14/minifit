.. minifit documentation master file, created by
   sphinx-quickstart on Thu Mar 23 23:13:44 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

MiniFit
#######

MiniFit is a Python package that uses ``scipy.optimize.curve_fit()`` to find optimal parameters of a chosen model function that best resembles the data.

.. toctree::
   :maxdepth: 1
   :numbered:
   :caption: User documentation

   minifit_overview


.. toctree::
   :maxdepth: 1
   :numbered:
   :caption: User documentation

   minifit_main

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
