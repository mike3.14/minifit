Download and Installation
=========================
MiniFit has been tested on Ubuntu 20.04 LTS.
It is available on PyPI and can be installed using pip.

.. code-block:: bash

     pip install minifit

Optionally, you can use Python ``venv`` when installing if you want to use MiniFit in an isolated environment.
SciPy, NumPy, matplotlib, and pathlib will be installed.

.. code-block:: bash

     cd folder_of_your_choice
     python3 -m venv env_minifit
     source env_minifit/bin/activate


     pip install minifit



You can also clone MiniFit by following these steps:


1. Create a directory to store MiniFit. For example:

.. code-block:: bash

    mkdir -p ${HOME}/my_packages
    cd ${HOME}/my_packages
    git clone https://gitlab.com/mike3.14/minifit.git

2. Install the package. To install MiniFit in development mode, use the ``-e`` flag. Make sure you are in the folder containing ``setup.py``:

.. code-block:: bash

    cd ${HOME}/my_packages/minifit
    pip install -e .

Now, MiniFit should be installed in editable/development mode. Any changes to the source code will take effect without requiring another installation.
