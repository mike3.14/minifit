Implemented Models.
===================


minifit.fit\_base module
------------------------

.. automodule:: minifit.fit_base
   :members:
   :undoc-members:
   :show-inheritance:


minifit.poly module
-------------------

.. automodule:: minifit.poly
   :members:
   :undoc-members:
   :show-inheritance:


minifit.morse module
--------------------

.. automodule:: minifit.morse
   :members:
   :undoc-members:
   :show-inheritance:


minifit.lennard\_jones module
-----------------------------

.. automodule:: minifit.lennard_jones
   :members:
   :undoc-members:
   :show-inheritance:


minifit.exponential module
--------------------------

.. automodule:: minifit.exponential
   :members:
   :undoc-members:
   :show-inheritance:


minifit.gauss module
--------------------

.. automodule:: minifit.gauss
   :members:
   :undoc-members:
   :show-inheritance:



minifit.user_defined module
----------------------------

.. automodule:: minifit.user_defined
   :members:
   :undoc-members:
   :show-inheritance:
