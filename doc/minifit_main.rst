Download and Installation
=========================

.. toctree::
   :maxdepth: 1

   instal


Curve fitting
=============

.. toctree::
   :maxdepth: 1

   fit_tutorial


Auto Docs
=========

.. toctree::
   :maxdepth: 1

   minifit
