MiniFit Overview
################

General description
===================

MiniFit is a Python package that uses scipy.optimize.curve_fit() to find optimal parameters of a chosen model function that best resembles the data.
Popt found will minimize model_function(xdata, \*popt) - ydata.


Implemented Features
====================

**Model functions**

* Polynomials

* Exponential

* Normal distribution

* Generalized Morse Potential

* Lennard-Jones potential

**User-defined model function**

* It's possible to pass a user-defined function to the ``UserFit`` class, and the new model will be used for fitting.

**Automatic fitting**

* Sometimes, the derivation of a good guess is impractical. Instead, MiniFit supports automatic fitting - meaning the package finds a good guess by itself automatically. Acceptable
  precision (maximum square root error of the fit) is defined by the user.

**Shifting data**

* Data can be shifted, making the convergence of the least squares algorithm easier.

**Quality of the fit**

To tell if the fit is good, one can look at:

* Graphs of the original data and results of the fit
* Differences between fit and data points
* Absolute sum of differences
* Square root error

**Representation of the results**

Numerical results are displayed in the terminal, and graphs are saved to pdfs.


To be released soon
===================


**More predefined model functions**

**Preproccesing of data**

**Postprocessing of the results**

* spectroscopic parameters, vibrational frequency, rotational constant
